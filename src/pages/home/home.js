import React from "react";
import { useQuery } from "@apollo/react-hooks";

import CharacterList from "components/characterList";
import Navbar from "components/navbar";
import Loader from "components/loader";
import { GET_SEARCH_TERM, GET_CHARACTERS } from "services/queries";

import "./home.scss";

const Home = () => {
    const {
        data: { searchTerm }
    } = useQuery(GET_SEARCH_TERM);

    const { loading, data } = useQuery(GET_CHARACTERS, {
        variables: { nameStartsWith: searchTerm },
        skip: searchTerm === ""
    });

    return (
        <div className="home" data-testid="home">
            <Navbar />
            {loading && <Loader />}
            {!loading && data && <CharacterList characters={data.characters} />}
        </div>
    );
};

export default Home;
