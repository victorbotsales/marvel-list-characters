import React, { useState } from "react";
import {
    Segment,
    Header,
    Grid,
    Divider,
    Button,
    Input
} from "semantic-ui-react";

import Navbar from "components/navbar";
import SerieList from "components/serieList";
import DataNotFound from "components/dataNotFound";
import useLocalStorage from "hooks/useLocalStorage";

import "./characterDetail.scss";

const CharacterDetail = ({
    location: {
        state: { character }
    }
}) => {
    const [{ name, thumbnail, series }, setCharacter] = useLocalStorage(
        character.id,
        character
    );
    const [tempCharacterName, setTempCharacterName] = useState(name);
    const [showEditInput, setShowEditInput] = useState(false);

    const onButtonClick = () => {
        if (showEditInput) {
            setCharacter({ ...character, name: tempCharacterName });
        }
        setShowEditInput(!showEditInput);
    };

    return (
        <div className="character-detail" data-testid="character-detail">
            <Navbar showSearchBar={false} />
            <Segment placeholder className="wrapper-grid">
                <Grid columns={2} stackable textAlign="center">
                    <Divider vertical />
                    <Grid.Row verticalAlign="middle">
                        <Grid.Column>
                            <div className="container-title-edit">
                                {showEditInput ? (
                                    <Input
                                        data-testid="input-character-name"
                                        placeholder="Character name"
                                        type="text"
                                        value={tempCharacterName}
                                        onChange={e =>
                                            setTempCharacterName(e.target.value)
                                        }
                                    />
                                ) : (
                                    <Header size="large" data-testid="header-character-name">{name}</Header>
                                )}
                                <Button
                                    secondary
                                    data-testid="button-edit"
                                    className="button-edit"
                                    onClick={onButtonClick}
                                >
                                    {!showEditInput ? "Edit" : "Save"}
                                </Button>
                            </div>
                            <img src={thumbnail} alt={name} />
                        </Grid.Column>

                        <Grid.Column>
                            {series.length ? (
                                <SerieList series={series} />
                            ) : (
                                <DataNotFound message="Series" />
                            )}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        </div>
    );
};

export default CharacterDetail;
