import React from "react";
import { Menu, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

import Searchbar from "components/searchbar";

import marvelLogo from "assets/images/marvel-logo.svg";
import "./navbar.scss";

const Navbar = ({ showSearchBar = true }) => (
    <Menu stackable inverted className="navbar" data-testid="navbar">
        <Menu.Item>
            <Link to="/">
                <Image src={marvelLogo} alt="Marvel Logo" />
            </Link>
        </Menu.Item>
        {showSearchBar && (
            <Menu.Item>
                <Searchbar />
            </Menu.Item>
        )}
    </Menu>
);

export default Navbar;
