import React from "react";
import { Header } from "semantic-ui-react";

import sadDeadpool from "assets/images/sad-deadpool.png";
import "./dataNotFound.scss";

const DataNotFound = ({ message }) => (
    <div className="notfound" data-testid="notfound">
        <Header size="huge">{message} Not Found!</Header>
        <img src={sadDeadpool} alt="Sad Deadpool" className="image-notfound" />
    </div>
);

export default DataNotFound;
