import React from "react";

import CharacterItem from "components/characterItem";
import DataNotFound from "components/dataNotFound";

import "./characterList.scss";

const CharacterList = ({ characters }) => {
    return (
        <React.Fragment>
            {characters.length ? (
                <div className="cards" data-testid="characters">
                    {characters.map(character => (
                        <CharacterItem
                            key={character.id}
                            character={character}
                        />
                    ))}
                </div>
            ) : (
                <DataNotFound
                    message="Characters"
                />
            )}
        </React.Fragment>
    );
};

export default CharacterList;
