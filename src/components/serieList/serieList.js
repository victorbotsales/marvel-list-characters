import React from "react";
import { List, Header } from "semantic-ui-react";

import SerieItem from "components/serieItem";

const SerieList = ({ series }) => (
    <div data-testid="series">
        <Header size="large">Series</Header>
        <List divided relaxed verticalAlign="top">
            {series.map((serie, index) => (
                <SerieItem key={index} serie={serie} />
            ))}
        </List>
    </div>
);

export default SerieList;
