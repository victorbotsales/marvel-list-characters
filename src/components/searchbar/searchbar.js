import React, { useState, useEffect } from "react";
import { Input } from "semantic-ui-react";
import { useApolloClient } from "@apollo/react-hooks";

import useDebounce from "hooks/useDebounce";

const Searchbar = () => {
    const [searchTerm, setSearchTerm] = useState("");
    const debouncedSearchTerm = useDebounce(searchTerm, 500);
    const client = useApolloClient();

    useEffect(() => {
        if (debouncedSearchTerm) {
            client.writeData({ data: { searchTerm: debouncedSearchTerm } });
        }
    }, [debouncedSearchTerm, client]);

    return (
        <Input
            data-testid="searchbar"
            value={searchTerm}
            className="icon"
            icon="search"
            placeholder="Search Character..."
            onChange={event => setSearchTerm(event.target.value)}
        />
    );
};

export default Searchbar;
