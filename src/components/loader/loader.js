import React from "react";

import marvelLoader from "assets/images/marvel-loader.gif";
import "./loader.scss";

const Loader = () => (
    <div className="wrapper-loader" data-testid="loader">
        <img src={marvelLoader} alt="Marvel Loader" className="loader" />
    </div>
);

export default Loader;
