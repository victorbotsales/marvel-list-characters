import React from "react";
import { List } from "semantic-ui-react";

const SerieItem = ({ serie }) => (
    <List.Item data-testid="serie-item">
        <List.Content>
            <List.Header>{serie.name}</List.Header>
        </List.Content>
    </List.Item>
);

export default SerieItem;
