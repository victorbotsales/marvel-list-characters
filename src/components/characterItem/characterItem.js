import React from "react";
import { Card, Image } from "semantic-ui-react";
import { withRouter } from "react-router-dom";

import useLocalStorage from "hooks/useLocalStorage";
import { slugify } from "utils/helpers";

import "./characterItem.scss";

const CharacterItem = ({ character, history }) => {
    const [{ name, thumbnail }] = useLocalStorage(character.id, character);

    const onCardClick = () => {
        history.push({
            pathname: `/character/${slugify(name)}`,
            state: {
                character
            }
        });
    };

    return (
        <Card onClick={onCardClick} data-testid="character">
            <Image src={thumbnail} ui={false} className="card-image" />
            <Card.Content>
                <Card.Header>{name}</Card.Header>
            </Card.Content>
        </Card>
    );
};

export default withRouter(CharacterItem);
