import React from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "pages/home";
import CharacterDetail from "pages/characterDetail";
import client from "services/apollo";

const App = () => {
    return (
        <ApolloProvider client={client}>
            <Router>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route
                        path="/character/:id"
                        exact
                        component={CharacterDetail}
                    />
                </Switch>
            </Router>
        </ApolloProvider>
    );
};

export default App;
