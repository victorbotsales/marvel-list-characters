import { localStorageWrapper } from "./helpers";

const NAMESPACE = "store";

const store = {
    set: (id, item) => {
        const items = localStorageWrapper.get(NAMESPACE) || {};
        items[id] = item;

        localStorageWrapper.set(NAMESPACE, items);
    },
    get: id => (localStorageWrapper.get(NAMESPACE) || {})[id] || null
};

export default store;
