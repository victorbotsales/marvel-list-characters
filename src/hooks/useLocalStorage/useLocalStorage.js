import { useState } from "react";
import store from "utils/store";

const useLocalStorage = (key, defaultValue) => {
    const [item, setItem] = useState(() => {
        try {
            const itemOnLocalStorage = store.get(key);
            return itemOnLocalStorage || defaultValue;
        } catch (error) {
            return defaultValue;
        }
    });

    const setItemOnLocalStorage = newItem => {
        setItem(newItem);
        store.set(key, newItem);
    };

    return [item, setItemOnLocalStorage];
};

export default useLocalStorage;
