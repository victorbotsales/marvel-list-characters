import gql from "graphql-tag";

export const GET_CHARACTERS = gql`
    query Characters($nameStartsWith: String!) {
        characters(where: { nameStartsWith: $nameStartsWith }) {
            id
            name
            thumbnail
            series {
                name
            }
        }
    }
`;

export const GET_SEARCH_TERM = gql`
    query GetSearchTerm {
        searchTerm @client
    }
`;
