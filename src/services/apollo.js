import ApolloClient, { InMemoryCache } from "apollo-boost";
import gql from "graphql-tag";

export const typeDefs = gql`
    extend type Query {
        searchTerm: String!
    }
`;

const resolvers = {};

const cache = new InMemoryCache();

const client = new ApolloClient({
    cache,
    uri: "https://api.marvelql.com/",
    typeDefs,
    resolvers
});

// Init Local State Data
cache.writeData({
    data: {
        searchTerm: ""
    }
});

export default client;
