import React from "react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import Navbar from "components/navbar";
import { MockedProvider } from "@apollo/react-testing";

afterEach(cleanup);

test("should render Navbar with SearchBar", async () => {
    const { getByTestId } = render(
        <MockedProvider mock={[]}>
            <MemoryRouter>
                <Navbar />
            </MemoryRouter>
        </MockedProvider>
    );
    expect(getByTestId("navbar"));
    expect(getByTestId("searchbar"));
});

test("should render Navbar without SearchBar", async () => {
    const { getByTestId, queryByTestId } = render(
        <MemoryRouter>
            <Navbar showSearchBar={false} />
        </MemoryRouter>
    );
    expect(getByTestId("navbar"));
    expect(queryByTestId("searchbar")).toBeNull();
});
