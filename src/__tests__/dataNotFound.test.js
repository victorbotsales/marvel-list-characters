import React from "react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import DataNotFound from "components/dataNotFound";

afterEach(cleanup);

test("should render DataNotFound with message", async () => {
    const message = "Characters";
    const { getByTestId, getByText } = render(
        <MemoryRouter>
            <DataNotFound message={message} />
        </MemoryRouter>
    );
    expect(getByTestId("notfound"));
    expect(getByText(/Characters/));
});
