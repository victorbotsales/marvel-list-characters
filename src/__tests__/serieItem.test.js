import React from "react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import SerieItem from "components/serieItem";

afterEach(cleanup);

const serie = {
    name: "Test serie"
};

test("should render SerieItem", async () => {
    const { getByTestId, getByText } = render(
        <MemoryRouter>
            <SerieItem serie={serie} />
        </MemoryRouter>
    );
    expect(getByTestId("serie-item"));
    expect(getByText(serie.name));
});
