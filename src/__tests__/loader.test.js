import React from "react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import Loader from "components/loader";

afterEach(cleanup);

test("should render Loader", async () => {
    const { getByTestId } = render(
        <MemoryRouter>
            <Loader />
        </MemoryRouter>
    );
    expect(getByTestId("loader"));
});
