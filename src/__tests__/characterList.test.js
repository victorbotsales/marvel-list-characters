import React from "react";
import { MockedProvider } from "@apollo/react-testing";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { GET_CHARACTERS } from "services/queries";
import CharacterList from "components/characterList";
import { render, cleanup } from "@testing-library/react";

afterEach(cleanup);

const resultWithCharacters = {
    data: {
        characters: [
            {
                id: "1010727",
                name: "Spider-dok",
                thumbnail:
                    "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg",
                series: [],
                __typename: "Character"
            },
            {
                id: "1009157",
                name: "Spider-Girl (Anya Corazon)",
                thumbnail:
                    "http://i.annihil.us/u/prod/marvel/i/mg/a/10/528d369de3e4f.jpg",
                series: [
                    {
                        name: "Amazing Fantasy (2004 - 2006)",
                        __typename: "Summary"
                    },
                    {
                        name: "Amazing Spider-Man (1999 - 2013)",
                        __typename: "Summary"
                    },
                    {
                        name: "Araña Vol. 1: Heart of the Spider (2005)",
                        __typename: "Summary"
                    },
                    {
                        name: "Avengers Assemble (2012 - 2014)",
                        __typename: "Summary"
                    },
                    {
                        name: "Captain America (2004 - 2011)",
                        __typename: "Summary"
                    },
                    {
                        name:
                            "Marvel Super Hero Adventures: Spider-Man - Across the Spider-Verse (2019)",
                        __typename: "Summary"
                    },
                    {
                        name:
                            "Marvel Super Hero Adventures: Spider-Man - Web of Intrigue (2019)",
                        __typename: "Summary"
                    },
                    {
                        name: "Onslaught Unleashed (2010 - 2011)",
                        __typename: "Summary"
                    },
                    {
                        name: "Spider-Girl (2010 - 2011)",
                        __typename: "Summary"
                    },
                    {
                        name: "Spider-Girls (2018)",
                        __typename: "Summary"
                    },
                    {
                        name: "Spider-Island: The Amazing Spider-Girl (2011)",
                        __typename: "Summary"
                    },
                    {
                        name:
                            "SPIDER-MAN: THE GAUNTLET VOL. 5 - LIZARD  (2011)",
                        __typename: "Summary"
                    },
                    {
                        name: "Spider-Man: You're Hired! (2011)",
                        __typename: "Summary"
                    },
                    {
                        name: "Web Warriors (2015 - 2016)",
                        __typename: "Summary"
                    }
                ],
                __typename: "Character"
            }
        ]
    }
};

const resultWithoutCharacters = {
    data: {
        characters: []
    }
};

test("Characters found", async () => {
    const mocks = [
        {
            request: {
                query: GET_CHARACTERS,
                variables: {
                    nameStartsWith: "spider"
                }
            },
            result: resultWithCharacters
        }
    ];
    const { characters } = resultWithCharacters.data;
    const { getByTestId } = render(
        <MockedProvider mocks={mocks}>
            <MemoryRouter>
                <CharacterList characters={characters} />
            </MemoryRouter>
        </MockedProvider>
    );
    expect(getByTestId("characters"));
});

test("Characters not found", async () => {
    const mocks = [
        {
            request: {
                query: GET_CHARACTERS,
                variables: {
                    nameStartsWith: "dasdDSadsa"
                }
            },
            result: resultWithoutCharacters
        }
    ];
    const { characters } = resultWithoutCharacters.data;
    const { getByTestId } = render(
        <MockedProvider mocks={mocks}>
            <MemoryRouter>
                <CharacterList characters={characters} />
            </MemoryRouter>
        </MockedProvider>
    );
    expect(getByTestId("notfound"));
});
