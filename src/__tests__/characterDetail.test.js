import React from "react";
import { createMemoryHistory } from 'history'
import { Router, Route } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup, fireEvent, waitForElement, getByPlaceholderText, getByText } from "@testing-library/react";
import CharacterDetail from "pages/characterDetail";
import { slugify } from "utils/helpers";

afterEach(cleanup);

const characterWithoutSeries = {
    id: "1010727",
    name: "Spider-dok",
    thumbnail:
        "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg",
    series: [],
    __typename: "Character"
};

const characterWithSeries = {
    id: "1010727",
    name: "Spider-dok",
    thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg",
    series: [
        {
            name: "Amazing Fantasy (2004 - 2006)",
            __typename: "Summary"
        },
        {
            name: "Amazing Spider-Man (1999 - 2013)",
            __typename: "Summary"
        },
        {
            name: "Araña Vol. 1: Heart of the Spider (2005)",
            __typename: "Summary"
        }
    ],
    __typename: "Character"
};


test("should render CharacterDetail without series", async () => {
    const history = createMemoryHistory();
    history.push({
        pathname: `/character/${slugify(characterWithoutSeries.name)}`,
        state: {
            character: characterWithoutSeries
        }
    });
    const { getByTestId } = render(
        <Router history={history}>
            <Route path="/character/:id" exact component={CharacterDetail} />
        </Router>
    );
    expect(getByTestId("character-detail"));
    expect(getByTestId("notfound"));
});

test("should render CharacterDetail with series", async () => {
    const history = createMemoryHistory();
    history.push({
        pathname: `/character/${slugify(characterWithoutSeries.name)}`,
        state: {
            character: characterWithSeries
        }
    });
    const { getByTestId } = render(
        <Router history={history}>
            <Route path="/character/:id" exact component={CharacterDetail} />
        </Router>
    );
    expect(getByTestId("character-detail"));
    expect(getByTestId("series"));
});

test("should render CharacterDetail changing Character's name when click on Edit Button", async () => {
    const history = createMemoryHistory();
    const characterName = "new character name";
    history.push({
        pathname: `/character/${slugify(characterWithoutSeries.name)}`,
        state: {
            character: characterWithoutSeries
        }
    });
    const { getByTestId, getByPlaceholderText } = render(
        <Router history={history}>
            <Route path="/character/:id" exact component={CharacterDetail} />
        </Router>
    );
    expect(getByTestId("character-detail"));
    const editButton = getByTestId("button-edit");
    fireEvent.click(editButton);
    const characterNameInput = getByPlaceholderText("Character name");
    fireEvent.change(characterNameInput, { target: { value: characterName} });
    expect(characterNameInput.value).toBe(characterName);
    fireEvent.click(editButton);
    expect(getByTestId("header-character-name").textContent).toBe(characterName);
});