import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import {
    render,
    cleanup,
    fireEvent,
    waitForElement
} from "@testing-library/react";
import CharacterItem from "components/characterItem";
import CharacterDetail from "pages/characterDetail";

afterEach(cleanup);

const character = {
    id: "1010727",
    name: "Spider-dok",
    thumbnail:
        "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg",
    series: [],
    __typename: "Character"
};

test("should render CharacterItem", async () => {
    const { getByTestId, getByText } = render(
        <MemoryRouter>
            <CharacterItem character={character} />
        </MemoryRouter>
    );
    expect(getByTestId("character"));
    expect(getByText(character.name));
});

test("should render CharacterDetail when click on CharacterItem", async () => {
    const { getByTestId, getByText } = render(
        <MemoryRouter>
            <Route path="/character/:id" exact component={CharacterDetail} />
            <CharacterItem character={character} />
        </MemoryRouter>
    );
    expect(getByTestId("character"));
    expect(getByText(character.name));
    fireEvent.click(getByTestId("character"));
    await waitForElement(() => getByTestId("character-detail"));
});
