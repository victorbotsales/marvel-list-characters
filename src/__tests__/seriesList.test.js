import React from "react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import SerieList from "components/serieList";

afterEach(cleanup);

const series = [
    {
        name: "Test serie"
    },
    {
        name: "Test serie2"
    }
];

test("should render SerieList", async () => {
    const { getAllByTestId, getByTestId } = render(
        <MemoryRouter>
            <SerieList series={series} />
        </MemoryRouter>
    );
    expect(getByTestId("series"));
    expect(getAllByTestId("serie-item")).toHaveLength(series.length);
});
