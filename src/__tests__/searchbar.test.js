import React from "react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup, fireEvent } from "@testing-library/react";
import Searchbar from "components/searchbar";
import { MockedProvider } from "@apollo/react-testing";

afterEach(cleanup);

test("should render Searchbar", async () => {
    const { getByTestId } = render(
        <MockedProvider mocks={[]}>
            <MemoryRouter>
                <Searchbar />
            </MemoryRouter>
        </MockedProvider>
    );
    expect(getByTestId("searchbar"));
});

test("should render Searchbar changing input", async () => {
    const { getByTestId, getByPlaceholderText } = render(
        <MockedProvider mocks={[]}>
            <MemoryRouter>
                <Searchbar />
            </MemoryRouter>
        </MockedProvider>
    );
    expect(getByTestId("searchbar"));
    const value = "spider test";
    const input = getByPlaceholderText(/Character/i);
    fireEvent.change(input, { target: { value } });
    expect(input.value).toBe(value);
});
