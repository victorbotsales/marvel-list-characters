import React from "react";
import { MockedProvider } from "@apollo/react-testing";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import Home from "pages/home";
import { render, cleanup, act } from "@testing-library/react";
import wait from "waait";
import { GET_CHARACTERS } from "services/queries";
import { InMemoryCache } from "apollo-boost";

afterEach(cleanup);

const mocks = [
    {
        request: {
            query: GET_CHARACTERS,
            variables: { nameStartsWith: "spider" }
        },
        result: {
            data: {
                characters: [
                    {
                        id: "1010727",
                        name: "Spider-dok",
                        thumbnail:
                            "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg",
                        series: [],
                        __typename: "Character"
                    },
                    {
                        id: "1009157",
                        name: "Spider-Girl (Anya Corazon)",
                        thumbnail:
                            "http://i.annihil.us/u/prod/marvel/i/mg/a/10/528d369de3e4f.jpg",
                        series: [
                            {
                                name: "Amazing Fantasy (2004 - 2006)",
                                __typename: "Summary"
                            },
                            {
                                name: "Amazing Spider-Man (1999 - 2013)",
                                __typename: "Summary"
                            },
                            {
                                name:
                                    "Araña Vol. 1: Heart of the Spider (2005)",
                                __typename: "Summary"
                            },
                            {
                                name: "Avengers Assemble (2012 - 2014)",
                                __typename: "Summary"
                            },
                            {
                                name: "Captain America (2004 - 2011)",
                                __typename: "Summary"
                            },
                            {
                                name:
                                    "Marvel Super Hero Adventures: Spider-Man - Across the Spider-Verse (2019)",
                                __typename: "Summary"
                            },
                            {
                                name:
                                    "Marvel Super Hero Adventures: Spider-Man - Web of Intrigue (2019)",
                                __typename: "Summary"
                            },
                            {
                                name: "Onslaught Unleashed (2010 - 2011)",
                                __typename: "Summary"
                            },
                            {
                                name: "Spider-Girl (2010 - 2011)",
                                __typename: "Summary"
                            },
                            {
                                name: "Spider-Girls (2018)",
                                __typename: "Summary"
                            },
                            {
                                name:
                                    "Spider-Island: The Amazing Spider-Girl (2011)",
                                __typename: "Summary"
                            },
                            {
                                name:
                                    "SPIDER-MAN: THE GAUNTLET VOL. 5 - LIZARD  (2011)",
                                __typename: "Summary"
                            },
                            {
                                name: "Spider-Man: You're Hired! (2011)",
                                __typename: "Summary"
                            },
                            {
                                name: "Web Warriors (2015 - 2016)",
                                __typename: "Summary"
                            }
                        ],
                        __typename: "Character"
                    }
                ]
            }
        }
    }
];
const cache = new InMemoryCache();
// Init Local State Data
cache.writeData({
    data: {
        searchTerm: "spider"
    }
});

test("should render loading state initially", async () => {
    await act(async () => {
        const { getByTestId } = render(
            <MockedProvider mocks={mocks} cache={cache}>
                <MemoryRouter>
                    <Home />
                </MemoryRouter>
            </MockedProvider>
        );
        expect(getByTestId("loader"));
    });
});

test("should render characters", async () => {
    await act(async () => {
        const { getByTestId } = render(
            <MockedProvider mocks={mocks} cache={cache}>
                <MemoryRouter>
                    <Home />
                </MemoryRouter>
            </MockedProvider>
        );
        await wait(0);
        expect(getByTestId("characters"));
    });
});
